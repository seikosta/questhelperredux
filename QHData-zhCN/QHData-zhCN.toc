## Interface: 50100
## Title: QuestHelper Data (zhCN)
## Author: zorbathut, smariot, vipersniper, nesher, nconantj
## Notes: Contains the data needed by QuestHelper
## Version: 4.0.1.$svnversion$
## Dependencies: QHData-base
## LoadOnDemand: 1

static_zhCN.lua
