## Interface: 50100
## Title: QuestHelper Data (esMX)
## Author: zorbathut, smariot, vipersniper, nesher, nconantj
## Notes: Contains the data needed by QuestHelper
## Version: 4.0.1.$svnversion$
## Dependencies: QHData-base
## LoadOnDemand: 1

static_esMX.lua
static_esMX_1.lua
static_esMX_2.lua
