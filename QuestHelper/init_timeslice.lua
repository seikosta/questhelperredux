QuestHelper_File["init_timeslice.lua"] = "4.0.1.$svnversion$"
QuestHelper_Loadtime["init_timeslice.lua"] = GetTime()

local GetTime = QuestHelper_GetTime

function QH_Init_Timeslice()
  QuestHelper_Loadtime["init3_start"] = GetTime()

  QuestHelper.loading_main = QuestHelper.CreateLoadingCounter()

  QuestHelper.loading_init3 = QuestHelper.loading_main:MakeSubcategory(0.3)
  QuestHelper.loading_flightpath = QuestHelper.loading_main:MakeSubcategory(1)
  QuestHelper.loading_preroll = QuestHelper.loading_main:MakeSubcategory(1)

  local stt = 0

  -- This is where the slow stuff goes
  -- 4.0.3a Breakage related
  local datime = time() + 30 -- We're gonna wait 30 seconds, just in case.
  --while datime >= time() do --[[sleep (busy wait)]] end
  --QuestHelper_BuildZoneLookup()
  --QH_Graph_Init()
  --load_graph_links()

  local self = QuestHelper -- whee hack hack hack

  if QuestHelper_Locale ~= GetLocale() then
    self:TextOut(QHText("LOCALE_ERROR"))
    return
  end
--[[
  if not self:ZoneSanity() then
    self:TextOut(QHFormat("ZONE_LAYOUT_ERROR", expected_version))
    QH_fixedmessage(QHFormat("ZONE_LAYOUT_ERROR", expected_version))
    QuestHelper = nil
    return
  end

  QuestHelper_UpgradeDatabase(_G)
  QuestHelper_UpgradeComplete()

  if QuestHelper_IsPolluted(_G) then
    self:TextOut(QHFormat("NAG_POLLUTED"))
    self:Purge(nil, true, true)
  end
--]]
  local signature = expected_version .. " on " .. GetBuildInfo()
  QuestHelper_Quests[signature] = QuestHelper_Quests[signature] or {}
  QuestHelper_Objectives[signature] = QuestHelper_Objectives[signature] or {}
  QuestHelper_FlightInstructors[signature] = QuestHelper_FlightInstructors[signature] or {}
  QuestHelper_FlightRoutes[signature] = QuestHelper_FlightRoutes[signature] or {}

  QuestHelper_Quests_Local = QuestHelper_Quests[signature]
  QuestHelper_Objectives_Local = QuestHelper_Objectives[signature]
  QuestHelper_FlightInstructors_Local = QuestHelper_FlightInstructors[signature]
  QuestHelper_FlightRoutes_Local = QuestHelper_FlightRoutes[signature]

  QuestHelper_SeenRealms[GetRealmName()] = true -- some attempt at tracking private servers

  QuestHelper.loading_init3:SetPercentage(0.1)
  QH_Collector_Init()
  QuestHelper.loading_init3:SetPercentage(0.5)
  --DB_Init()
  QuestHelper.loading_init3:SetPercentage(0.9)

  self.player_level = UnitLevel("player")

  self:SetLocaleFonts()

  if QuestHelper_Pref.share and not QuestHelper_Pref.solo then
    self:EnableSharing()
  end

  if QuestHelper_Pref.hide then
    self.map_overlay:Hide()
  end

  self:HandlePartyChange()

  self:Nag("all")

  for locale in pairs(QuestHelper_StaticData) do
    if locale ~= self.locale then
      -- Will delete references to locales you don't use.
      QuestHelper_StaticData[locale] = nil
      _G["QuestHelper_StaticData_" .. locale] = nil
    end
  end

  local static = QuestHelper_StaticData[self.locale]

  if static then
    if static.flight_instructors then for faction in pairs(static.flight_instructors) do
      if faction ~= self.faction then
        -- Will delete references to flight instructors that don't belong to your faction.
        static.flight_instructors[faction] = nil
      end
    end end

    if static.quest then for faction in pairs(static.quest) do
      if faction ~= self.faction then
        -- Will delete references to quests that don't belong to your faction.
        static.quest[faction] = nil
      end
    end end
  end

  -- Adding QuestHelper_CharVersion, so I know if I've already converted this characters saved data.
  if not QuestHelper_CharVersion then
    -- Changing per-character flight routes, now only storing the flight points they have,
    -- will attempt to guess the routes from this.
    local routes = {}

    for i, l in pairs(QuestHelper_KnownFlightRoutes) do
      for key in pairs(l) do
        routes[key] = true
      end
    end

    QuestHelper_KnownFlightRoutes = routes

    -- Deleting the player's home again.
    -- But using the new CharVersion variable I'm adding is cleaner that what I was doing, so I'll go with it.
    QuestHelper_Home = nil
    QuestHelper_CharVersion = 1
  end

  if not QuestHelper_Home then
    -- Not going to bother complaining about the player's home not being set, uncomment this when the home is used in routing.
    -- self:TextOut(QHText("HOME_NOT_KNOWN"))
  end

  if QuestHelper_Pref.map_button then
--        QuestHelper:InitMapButton()
  end

  if QuestHelper_Pref.tomtom_wp_new then
--      self:EnableTomTom()
  end

--    self.tracker:SetScale(QuestHelper_Pref.track_scale)

  local version = GetAddOnMetadata("QuestHelper", "Version") or "Unknown"

  local major, minor = (version_string or ""):match("^(%d+)%.(%d+)")
  major, minor = tonumber(major), tonumber(minor)

  QH_Hook(self, "OnUpdate", self.OnUpdate)

  -- Seems to do its own garbage collection pass before fully loading, so I'll just rely on that
  --collectgarbage("collect") -- Free everything we aren't using.

  --[[
  if self.debug_objectives then
    for name, data in pairs(self.debug_objectives) do
      self:LoadDebugObjective(name, data)
    end
  end]]

  -- wellllp
  QH_Arrow_SetScale()
  QH_Arrow_SetTextScale()

  --[[
  QH_Timeslice_Add(function ()
    self:ResetPathing()
    self.Routing:Initialize()       -- Set up the routing task
  end, "init")]] -- FUCK YOU BOXBOT

  QH_Event("CHAT_MSG_ADDON", function (...)
    local arg1 = select(1,...)
    local arg2 = select(2,...)
    local arg4 = select(4,...)
    if arg1 == "QHpr" and arg4 ~= UnitName("player") then
      QH_Questcomm_Msg(arg2, arg4)
    end
  end)

  --[[
  QH_Event({"PARTY_MEMBERS_CHANGED", "UNIT_LEVEL", "RAID_ROSTER_UPDATE"}, function ()
    QH_Filter_Group_Sync()
    QH_Route_Filter_Rescan("filter_quest_level")
    --QH_Route_Filter_Rescan("filter_quest_group")
    --QH_Route_Filter_Rescan("filter_quest_raid_accessible")  -- These should be in right now, but for simplicity's sake we're actually scanning everything when we get a rescan request. So they're unnecessary. PUT THEM BACK should they become necessary.
  end)

  QH_Event({"PARTY_MEMBERS_CHANGED", "RAID_ROSTER_UPDATE"}, function ()
    QH_Questcomm_Sync()
  end)
  --]]

  QH_Event("PLAYER_LEVEL_UP", function ()
    self.player_level = arg1
    --QH_Route_Filter_Rescan("filter_quest_level")
  end)
--[[
  QH_Event("TAXIMAP_OPENED", function ()
    self:taxiMapOpened()
  end)

  QH_Event({"ZONE_CHANGED", "ZONE_CHANGED_INDOORS", "ZONE_CHANGED_NEW_AREA"}, function()
    QH_Route_Filter_Rescan(nil, true)
  end)
]]
  QH_Event("CHAT_MSG_CHANNEL_NOTICE", function()
    if please_submit_enabled and not please_submit_initted then
      please_submit_enabled = QHNagInit()
      startup_time = GetTime()
      please_submit_initted = true
    end
  end)

  QuestHelper.loading_init3:SetPercentage(1.0)  -- victory

  QuestHelper_Loadtime["init3_end"] = GetTime()
end
