QuestHelper_File["pref.lua"] = "4.0.1.$svnversion$"
QuestHelper_Loadtime["pref.lua"] = GetTime()

QuestHelper_Pref = {}

QuestHelper_DefaultPref = {
  filter_level = true,
  filter_zone = false,
  filter_done = false,
  filter_blocked = false, -- Hides blocked objectives, such as quest turn-ins for incomplete quests
  filter_watched = false, -- Limits to watched objectives
  filter_group = true,
  filter_group_param = 2,
  filter_wintergrasp = true,
  filter_raid_accessible = true,
  zones = "next",
  track = true,
  track_scale = 1,
  track_level = true,
  track_qcolour = true,
  track_ocolour = true,
  track_size = 10,
  blizzmap = false,
  tooltip = true,
  share = true,
  scale = 1,
  solo = false,
  comm = false,
  show_ants = true,
  level = 3,
  hide = false,
  tomtom_wp_new = false,
  arrow = true,
  arrow_locked = false,
  arrow_arrowsize = 1,
  arrow_textsize = 1,
  metric = (QuestHelper_Locale ~= "enUS" and QuestHelper_Locale ~= "esMX"),
  flight_time = true,
  locale = GetLocale(), -- This variable is used for display purposes, and has nothing to do with the collected data.
  perf_scale_2 = 1, -- How much background processing can the current machine handle? Higher means more load, lower means better performance.
  perfload_scale = 1, -- Performance scale to use on startup.
  map_button = true,
  travel_time = false,
  mini_opacity = 1,
}

-- We do it here also in case things decide they care about preferences before the init function is called. Shouldn't happen, but maybe does.
setmetatable(QuestHelper_Pref,  {__index = QuestHelper_DefaultPref})
