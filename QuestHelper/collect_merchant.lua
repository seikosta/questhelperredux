
local GetTime = QuestHelper_GetTime

QuestHelper_File["collect_merchant.lua"] = "4.0.1.$svnversion$"
QuestHelper_Loadtime["collect_merchant.lua"] = GetTime()

-- http://www.penny-arcade.com/comic/2005/01/05/

local debug_output = false
if QuestHelper_File["collect_merchant.lua"] == "Development Version" then debug_output = true end

local QHCM

local IsMonsterGUID
local GetMonsterType
local GetItemType

local function AddChatType(typ)
  local target = UnitGUID("target")
  if not target then return end

  if not IsMonsterGUID(target) then return end

  target = GetMonsterType(target)

  local ret = {}
  ret.base_info = string.format("%d\t%s", target, typ)
  table.insert(QHCM, ret)
  return ret
end

local merchant_shown = {}
local session_currencies = {}

local function MerchantUpdate()
  local ct = GetMerchantNumItems()
  if merchant_shown[UnitGUID("target")] then return end -- don't collect again.
  for i = 1, ct do
	if not GetMerchantItemLink(i) then
		return
	end
  end  -- We want to make sure it's cached, otherwise we'll return wonky data. Technically this biases things away from "yes he's a shopkeeper", but honestly, it doesn't matter that much.

  -- now, we haven't collected and everything is ready to go.
  merchant_shown[UnitGUID("target")] = true -- cache the shop keeper for the session. It is possible for the items to change, buy meh, we won't worry about it... they might come up in the next session.
  targ = AddChatType("shop")
  if not targ then return end -- welllllp

  local ct = GetMerchantNumItems()
  --QuestHelper:TextOut(string.format("nitems %d", ct))

  local currency_list = { GetMerchantCurrencies() }

  local currency_rev_list
  if #currency_list ~= 0 then
	for j = 1, #currency_list do
		local id = currency_list[j]
		if not session_currencies[id] then
			session_currencies[id] = true
			local name, _, icon = GetCurrencyInfo(currency_list[j])
			table.insert(QHCC, string.format("%d\t%s\t%s", id, name, icon))
		end
	end
  end

  for i = 1, ct do
    local itemid = GetMerchantItemLink(i)
    QuestHelper: Assert(itemid)
    itemid = GetItemType(itemid)
    local _, _, price, quant, avail, _, ext_cost = GetMerchantItemInfo(i)
	local shop_item = {}
	shop_item.base_info = string.format("%d\t%d\t%d\t%d", itemid, quant, avail, price)

	if ext_cost then
		ext_costs = {}
		for curr = 1, 3 do
			local tex, val, link, name = GetMerchantItemCostItem(i, curr)
			if tex then
				local base_info = string.format("%s\t%s\t%d\t%s", name, tex, val, link or "NO LINK")
				table.insert(ext_costs, base_info)
			end
		end
		shop_item.ext_costs = ext_costs
	end

    --if debug_output then QuestHelper:TextOut(dstr) end
    if not targ.shop then targ.shop = {} end

	table.insert(targ.shop, shop_item)
  end
end

local function GossipShow()
  AddChatType("talk")
end

local function QuestGreeting()
  AddChatType("quest_greet")
end

local function QuestComplete()
  AddChatType("quest_complete")
end

local function QuestDetail()
  AddChatType("quest_detail")
end

local function QuestFinished()
  AddChatType("quest_finished")
end

local function QuestProgress()
  AddChatType("quest_progress")
end


function QH_Collect_Merchant_Init(QHCData, API)
  if not QHCData.merchant then QHCData.merchant = {} end
  QHCM = QHCData.merchant

  if not QHCData.currencies then QHCData.currencies = {} end
  QHCC = QHCData.currencies

  QH_Event("MERCHANT_SHOW", MerchantUpdate)
  --QH_Event("MERCHANT_UPDATE", MerchantUpdate)
  QH_Event("GOSSIP_SHOW", GossipShow)
  QH_Event("QUEST_GREETING", QuestGreeting)
  QH_Event("QUEST_COMPLETE", QuestComplete)
  QH_Event("QUEST_DETAIL", QuestDetail)
  QH_Event("QUEST_FINISHED", QuestFinished)
  QH_Event("QUEST_PROGRESS", QuestProgress)

  IsMonsterGUID = API.Utility_IsMonsterGUID
  GetMonsterType = API.Utility_GetMonsterType
  GetItemType = API.Utility_GetItemType
  QuestHelper: Assert(IsMonsterGUID)
  QuestHelper: Assert(GetMonsterType)
  QuestHelper: Assert(GetItemType)
end
