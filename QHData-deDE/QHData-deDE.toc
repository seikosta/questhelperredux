## Interface: 50100
## Title: QuestHelper Data (deDE)
## Author: zorbathut, smariot, vipersniper, nesher, nconantj
## Notes: Contains the data needed by QuestHelper
## Version: 4.0.1.$svnversion$
## Dependencies: QHData-base
## LoadOnDemand: 1

static_deDE.lua
static_deDE_1.lua
static_deDE_2.lua
