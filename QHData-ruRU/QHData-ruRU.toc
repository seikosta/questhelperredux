## Interface: 50100
## Title: QuestHelper Data (ruRU)
## Author: zorbathut, smariot, vipersniper, nesher, nconantj
## Notes: Contains the data needed by QuestHelper
## Version: 4.0.1.$svnversion$
## Dependencies: QHData-base
## LoadOnDemand: 1

static_ruRU.lua
static_ruRU_1.lua
static_ruRU_2.lua
