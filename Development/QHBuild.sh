#!/bin/bash

release=a
version=0
issue=0
branch=trunk

if [ $# -eq 0 ]
then
  echo >&2 \
  "usage: $0 [-release a|b|r] [-version some_repository_version] [-issue an_issue_number] [-branch trunk|some_branch]"
  exit 1
fi

while [ $# -gt 0 ]
do
  case "$1" in
    -release) release="$2"; shift;;
    -version) version="$2"; shift;;
    -issue) issue="$2"; shift;;
    -branch) branch="$2"; shift;;
    -*) echo >&2 \
        "usage: $0 [-release a|b|r] [-version some_repository_version] [-issue an_issue_number] [-branch trunk|some_branch]"
        exit 1;;
    *) break;; # terminate while loop
  esac
  shift
done

if [ $release != "a" -a $release != "b" -a $release != "r" ]
then
  echo >&2 \
  "Release must be 'a', 'b' or 'r'."
  echo >&2 \
  "usage: $0 [-release a|b|r] [-version some_repository_version] [-issue an_issue_number] [-branch trunk|some_branch]"
  exit 1
fi

if [ $version -ne $version 2> /dev/null ]
then
  echo >&2 \
  "Version must be a number."
  echo >&2 \
  "usage: $0 [-release a|b|r] [-version some_repository_version] [-issue an_issue_number] [-branch trunk|some_branch]"
  exit 1
fi

if [ $issue -ne $issue 2> /dev/null ]
then
  echo >&2 \
  "Issue must be a number."
  echo >&2 \
  "usage: $0 [-release a|b|r] [-version some_repository_version] [-issue an_issue_number] [-branch trunk|some_branch]"
  exit 1
fi

if [ $branch != "trunk" ]
then
  branch=branches/$branch
  svn ls https://questhelperredux.googlecode.com/svn/$branch >/dev/null 2>&1
  if [ $? -ne 0 ]
  then
    echo >&2 \
    "Branch exists."
    echo >&2 \
    "usage: $0 [-release a|b|r] [-version some_repository_version] [-issue an_issue_number] [-branch trunk|some_branch]"
    exit 1
  fi
fi

command=
if [ $version -gt 0 ]
then
  command="export -r $version https://questhelperredux.googlecode.com/svn/$branch build"
else
  command="export https://questhelperredux.googlecode.com/svn/$branch build"

  cd trunk
  let version=`svnversion`
  cd ..
fi

echo Exporting files.
svn $command

if [ $? -ne 0 ]
then
	echo Build fails. Cleaning up.
	cd ..
	rm -rvf build
	echo "Build failed. Repository export failed."
	return 1
fi

cd build

release_string=${version}${release}

echo Replacing version tag and creating package.
if [ $issue -gt 0 ]
then
  release_string = "${release_string}-i${issue}"
fi

sub="s/\\\$svnversion\\\$/${release_string}/g"
sub2='s/4\.0\.1/5\.1\.0/'
sub3='s/PURGEDEV = false/PURGEDEV = true/'

for toc in `find . -iname "*.toc"`
do
  echo Replacing version tag in ${toc}.
  cp $toc $toc.bak
  sed -e $sub $toc.bak > $toc
  rm $toc.bak
  cp $toc $toc.bak
  sed -e $sub2 $toc.bak > $toc
  rm $toc.bak
done

for lua in `find . -iname "*.lua"`
do
  echo Replacing version tag in ${lua}.
  cp $lua $lua.bak
  sed -e $sub $lua.bak > $lua
  rm $lua.bak
  cp $lua $lua.bak
  sed -e $sub2 $lua.bak > $lua
  rm $lua.bak
done

echo Turning on PURGEDEV
cp collect.lua collect.lua.bak
sed -e $sub3 collect.lua.bak > collect.lua
rm collect.lua.bak

echo ${release_string}

zip -qdgds 1m -r QuestHelper-5.1.0.${release_string}.zip QuestHelper/ QHData-*

echo Moving the package up one directory level.
mv -v *.zip ..

cd ..

echo Cleaning up.
rm -rvf build

echo Completed exporting and packaging revision $version
