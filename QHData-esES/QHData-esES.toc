## Interface: 50100
## Title: QuestHelper Data (esES)
## Author: zorbathut, smariot, vipersniper, nesher, nconantj
## Notes: Contains the data needed by QuestHelper
## Version: 4.0.1.$svnversion$
## Dependencies: QHData-base
## LoadOnDemand: 1

static_esES.lua
static_esES_1.lua
static_esES_2.lua
