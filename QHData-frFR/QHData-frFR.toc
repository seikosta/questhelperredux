## Interface: 50100
## Title: QuestHelper Data (frFR)
## Author: zorbathut, smariot, vipersniper, nesher, nconantj
## Notes: Contains the data needed by QuestHelper
## Version: 4.0.1.$svnversion$
## Dependencies: QHData-base
## LoadOnDemand: 1

static_frFR.lua
static_frFR_1.lua
static_frFR_2.lua
