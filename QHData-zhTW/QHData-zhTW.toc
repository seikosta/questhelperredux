## Interface: 50100
## Title: QuestHelper Data (zhTW)
## Author: zorbathut, smariot, vipersniper, nesher, nconantj
## Notes: Contains the data needed by QuestHelper
## Version: 4.0.1.$svnversion$
## Dependencies: QHData-base
## LoadOnDemand: 1

static_zhTW.lua
static_zhTW_1.lua
static_zhTW_2.lua
